﻿using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int left_factor, int right_factor){
            Console.Write("Multiplication de {0} par {1} ", left_factor, right_factor);
            return left_factor * right_factor;
        }
        public static int Addition(int left_factor,int right_factor){
            Console.Write("Addition de {0} à {1} ", right_factor, left_factor);
            return left_factor + right_factor;
        }
        public static int Soustraction(int left_factor, int right_factor){
            Console.Write("Soustraction de {0} par {1} ", right_factor, left_factor);
            return left_factor - right_factor;
        }
        public static float Division(float left_f_factor, float right_f_factor)
        {
            Console.Write("Division de {0} par {1} : ", left_f_factor,right_f_factor);
            Console.WriteLine(left_f_factor / right_f_factor);
            return left_f_factor / right_f_factor;
        }
        static void Main(string[] args)
        {
            int left_factor, right_factor, result;
            float decimal_result,left_f_factor, right_f_factor;
            Console.WriteLine("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*");
            Console.WriteLine("*_*_*_*_*_*_*_*_*CALCULATRICE  K2000*_*_*_*_*_*_*_*_*");
            Console.WriteLine("*_*_*_*_*_*_*Created by Hugo Benabdelhak*_*_*_*_*_*_*");
            Console.WriteLine("*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*_*");
            Console.WriteLine("Fonctionnalités 1 : Multiplication");
            Console.WriteLine("Fonctionnalités 2 : Addition");
            Console.WriteLine("Fonctionnalités 3 : Division");
            Console.WriteLine("Fonctionnalités 4 : Soustraction");
            Console.WriteLine("Saisissez la fonction que vous souhaitez utiliser : ");
            int choice_from_user = int.Parse(Console.ReadLine());
            switch(choice_from_user)
            {
                case 1:
                    Console.WriteLine("*_*_*_*_*_*_*MULTIPLICATION*_*_*_*_*_*_*");
                    Console.WriteLine("Saisissez le premier facteur puis, le second.");
                    left_factor = int.Parse(Console.ReadLine());
                    right_factor = int.Parse(Console.ReadLine());
                    result = Multiplication(left_factor, right_factor);
                    Console.WriteLine(result);
                    break;
                case 2:
                    Console.WriteLine("*_*_*_*_*_*_*ADDITION*_*_*_*_*_*_*");
                    Console.WriteLine("Saisissez le premier facteur puis, le second.");
                    left_factor = int.Parse(Console.ReadLine());
                    right_factor = int.Parse(Console.ReadLine());
                    result = Addition(left_factor, right_factor);
                    Console.WriteLine(result);
                    break;
                case 3:
                    Console.WriteLine("*_*_*_*_*_*_*DIVISION*_*_*_*_*_*_*");
                    Console.WriteLine("Saisissez le premier facteur puis, le second.");
                    left_f_factor = float.Parse(Console.ReadLine());
                    right_f_factor = float.Parse(Console.ReadLine());
                    decimal_result = Division(left_f_factor, right_f_factor);
                    break;
                case 4:
                    Console.WriteLine("*_*_*_*_*_*_*SOUSTRACTION*_*_*_*_*_*_*");
                    Console.WriteLine("Saisissez le premier facteur puis, le second.");
                    left_factor = int.Parse(Console.ReadLine());
                    right_factor = int.Parse(Console.ReadLine());
                    result = Soustraction(left_factor, right_factor);
                    Console.WriteLine(result);
                    break;
                default:
                    Console.WriteLine("La fonction saisie n'existe pas. Veuillez recommencer.");
                    break;
            }
        }
    }
}
