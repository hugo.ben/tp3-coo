using System;
using Xunit;

using CalculatriceConsole;

namespace CalculatriceTest
{
    public class OperationTests
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2, 2));
            Assert.Equal(21, Program.Multiplication(7, 3));
        }
        [Fact]
        public void TestAddtion()
        {
            Assert.Equal(9,Program.Addition(3,6));
            Assert.Equal(11,Program.Addition(5, 6));
        }
        [Fact]
        public void TestSoustraction()
        {
            Assert.Equal(5, Program.Soustraction(10, 5));
            Assert.Equal(-2,Program.Soustraction(4, 6));
        }
        [Fact]
        public void TestDivision()
        {
            Assert.Equal(2.5 , Program.Division(5, 2));
            Assert.Equal(1.25,Program.Division(10,8));
        }
    }
}
